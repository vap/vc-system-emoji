/*
 * Vencord, a Discord client mod
 * Copyright (c) 2023 Vendicated and contributors
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import "./style.css";

import { classNameFactory } from "@api/Styles";
import { Devs } from "@utils/constants";
import definePlugin from "@utils/types";

interface EmojiProps {
    node: {
        jumboable: boolean;
        name: string;
        src: string;
        surrogate: string;
    };
}

const cl = classNameFactory("vc-system-emoji-");

export default definePlugin({
    name: "System Emoji",
    description: "Replaces Discord's emoji with the system emoji.",
    tags: ["emoji"],
    authors: [Devs.Vap],

    patches: [{
        find: '.jumboable?"jumbo":"default"',
        replacement: {
            match: /(?<=var \i=function\((\i)\){)(?=.{0,300}animated:!1)/,
            replace: (_, props) => `return $self.renderEmoji(${props});`
        },
    }],

    renderEmoji({ node }: EmojiProps) {
        return <span className={cl("emoji", node.jumboable && "jumboable")}>{node.surrogate}</span>;
    },
});
